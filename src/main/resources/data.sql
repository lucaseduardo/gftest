------------------
-- Especialidades
------------------

insert into especialidade (descricao) values ('Cardiologia');
insert into especialidade (descricao) values ('Dermatologia');
insert into especialidade (descricao) values ('Ortopedia');
insert into especialidade (descricao) values ('Oftalmologia');

------------------------------------
-- prestadores de cardialogia
------------------------------------

insert into prestador (nome, cidade, bairro, endereco, latitude, longitude) values ('Clinimarc Cardiologia e Endocrinologia', 'Curitiba', 'Água Verde', 'R. Acyr Guimarães, 436', -25.449247, -49.290987);
insert into prestador (nome, cidade, bairro, endereco, latitude, longitude) values ('Sugisawa Cardiologia', 'Curitiba', 'Rebouças', 'R. Lamenha Lins, 769', -25.443659, -49.273671);
insert into prestador (nome, cidade, bairro, endereco, latitude, longitude) values ('Cardiocare Clínica Cardiológica', 'Curitiba', 'Jardim Social', 'Av. Mal. Humberto de Alencar Castelo Branco, 295', -25.430306, -49.239937);
insert into prestador (nome, cidade, bairro, endereco, latitude, longitude) values ('Hospital Cardiológico Costantini', 'Curitiba', 'Vila Izabel', 'Rua Pedro Collere, 890', -25.463130, -49.299655);
insert into prestador (nome, cidade, bairro, endereco, latitude, longitude) values ('Hospital Do Coração', 'Curitiba', 'Batel', 'R. Alferes Ângelo Sampaio, 1896', -25.437767, -49.286686);

insert into prestadores_especialidades (id_prestador, id_especialidade) values (1, 1);
insert into prestadores_especialidades (id_prestador, id_especialidade) values (2, 1);
insert into prestadores_especialidades (id_prestador, id_especialidade) values (3, 1);
insert into prestadores_especialidades (id_prestador, id_especialidade) values (4, 1);
insert into prestadores_especialidades (id_prestador, id_especialidade) values (5, 1);

------------------------------------
-- prestadores de dermatologia
------------------------------------

insert into prestador (nome, cidade, bairro, endereco, latitude, longitude) values ('Peleclin Clínica Dermatológica', 'Curitiba', 'Centro', 'R. Visc. do Rio Branco, 1358', -25.433683, -49.279750);
insert into prestador (nome, cidade, bairro, endereco, latitude, longitude) values ('Clínica ViaPelle Dermatologia', 'Curitiba', 'Centro', 'Rua Emiliano Perneta, 860', -25.438216, -49.279226);
insert into prestador (nome, cidade, bairro, endereco, latitude, longitude) values ('Instituto Dermatológico de Curitiba', 'Curitiba', 'Água Verde', 'Av. Silva Jardim, 3515', -25.449917, -49.293472);
insert into prestador (nome, cidade, bairro, endereco, latitude, longitude) values ('Clínica Jardins Dermatologista', 'Curitiba', 'Água Verde', 'Av. Silva Jardim, 2121', -25.444956, -49.280791);
insert into prestador (nome, cidade, bairro, endereco, latitude, longitude) values ('CEPELLE - Dermatologia em Curitiba', 'Curitiba', 'Batel', 'Av. Vicente Machado, 1907', -25.440528, -49.294324);

insert into prestadores_especialidades (id_prestador, id_especialidade) values (6, 2);
insert into prestadores_especialidades (id_prestador, id_especialidade) values (7, 2);
insert into prestadores_especialidades (id_prestador, id_especialidade) values (8, 2);
insert into prestadores_especialidades (id_prestador, id_especialidade) values (9, 2);
insert into prestadores_especialidades (id_prestador, id_especialidade) values (10, 2);

------------------------------------
-- prestadores de ortopedia
------------------------------------

insert into prestador (nome, cidade, bairro, endereco, latitude, longitude) values ('Clínica de Fraturas Torres', 'Curitiba', 'Uberaba', 'Av. Com. Franco, 2795', -25.458087, -49.239388);
insert into prestador (nome, cidade, bairro, endereco, latitude, longitude) values ('Clínica de Ortopedia Pediátrica', 'Curitiba', 'Água verde', 'R. Buenos Aires, 1020', -25.446144, -49.276544);
insert into prestador (nome, cidade, bairro, endereco, latitude, longitude) values ('Clínica Batel Ortopedia', 'Curitiba', 'Mercês', 'R. Padre Anchieta, 751', -25.428377, -49.287052);
insert into prestador (nome, cidade, bairro, endereco, latitude, longitude) values ('Clínica Paranaense de Ortopedia', 'Curitiba', 'Alto da XV', 'R. Prof. Brandão, 309', -25.423827, -49.252554);
insert into prestador (nome, cidade, bairro, endereco, latitude, longitude) values ('Clínica Fraturas Ortopedia Curitiba', 'Curitiba', 'Bigorrilho', 'Alameda Dr. Carlos de Carvalho, 2320', -25.439529, -49.296782);

insert into prestadores_especialidades (id_prestador, id_especialidade) values (11, 3);
insert into prestadores_especialidades (id_prestador, id_especialidade) values (12, 3);
insert into prestadores_especialidades (id_prestador, id_especialidade) values (13, 3);
insert into prestadores_especialidades (id_prestador, id_especialidade) values (14, 3);
insert into prestadores_especialidades (id_prestador, id_especialidade) values (15, 3);

------------------------------------
-- prestadores de oftalmologia
------------------------------------

insert into prestador (nome, cidade, bairro, endereco, latitude, longitude) values ('Instituto de Oftalmologia de Curitiba', 'Curitiba', 'Rebouças', 'Av. Pres. Getúlio Vargas, 1500', -25.445755, -49.274622);
insert into prestador (nome, cidade, bairro, endereco, latitude, longitude) values ('Clínica de Olhos Dr Carlos Tedeschi', 'Curitiba', 'Batel', 'R. Gabriel de Lara, 75', -25.445012, -49.296038);
insert into prestador (nome, cidade, bairro, endereco, latitude, longitude) values ('Médicos de Olhos S.A', 'Curitiba', 'Batel', 'R. Benjamin Lins, 790', -25.439796, -49.281775);
insert into prestador (nome, cidade, bairro, endereco, latitude, longitude) values ('OCULARIS Oftalmologia Avançada', 'Curitiba', 'Centro', 'R. Emiliano Perneta, 466 Quarto andar', -25.435737, -49.276280);
insert into prestador (nome, cidade, bairro, endereco, latitude, longitude) values ('CEVIPA OFTALMOLOGIA CURITIBA', 'Curitiba', 'Centro', 'Av. Vicente Machado, 467', -25.435049, -49.281279);

insert into prestadores_especialidades (id_prestador, id_especialidade) values (16, 4);
insert into prestadores_especialidades (id_prestador, id_especialidade) values (17, 4);
insert into prestadores_especialidades (id_prestador, id_especialidade) values (18, 4);
insert into prestadores_especialidades (id_prestador, id_especialidade) values (19, 4);
insert into prestadores_especialidades (id_prestador, id_especialidade) values (20, 4);

-----------------------------------------
-- prestador de todas as especialidades
-----------------------------------------

insert into prestador (nome, cidade, bairro, endereco, latitude, longitude) values ('Hospital Marcelino Champagnat', 'Curitiba', 'Cristo Rei', 'Av. Presidente Affonso Camargo, 1399', -25.435943, -49.246138);
insert into prestadores_especialidades (id_prestador, id_especialidade) values (21, 1);
insert into prestadores_especialidades (id_prestador, id_especialidade) values (21, 2);
insert into prestadores_especialidades (id_prestador, id_especialidade) values (21, 3);
insert into prestadores_especialidades (id_prestador, id_especialidade) values (21, 4);