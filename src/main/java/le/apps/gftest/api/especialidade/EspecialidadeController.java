package le.apps.gftest.api.especialidade;

import le.apps.gftest.api.especialidade.dto.EspecialidadeDTO;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/gftest/especialidade")
public class EspecialidadeController {

    @Autowired
    private EspecialidadeService especialidadeService;

    @Autowired
    private ModelMapper mapper;

    @GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<EspecialidadeDTO>> getEspecialidades() {

        List<Especialidade> especialidades = especialidadeService.getEspecialidades();
        List<EspecialidadeDTO> especialidadeDTO = mapper.map(especialidades, new TypeToken<List<EspecialidadeDTO>>() {}.getType());

        return ResponseEntity.ok(especialidadeDTO);
    }
}
