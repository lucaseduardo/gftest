package le.apps.gftest.api.especialidade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EspecialidadeService {

    @Autowired
    private EspecialidadeRepository especialidadeRepository;

    public List<Especialidade> getEspecialidades() {
        return (List<Especialidade>) especialidadeRepository.findAll();
    }

    public Especialidade getEspecialidade(long id) {
        return especialidadeRepository.findById(id).get();
    }
}
