package le.apps.gftest.api.especialidade;

import org.springframework.data.repository.CrudRepository;

public interface EspecialidadeRepository extends CrudRepository<Especialidade,Long> {
}
