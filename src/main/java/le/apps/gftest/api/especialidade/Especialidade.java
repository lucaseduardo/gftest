package le.apps.gftest.api.especialidade;

import com.fasterxml.jackson.annotation.JsonBackReference;
import le.apps.gftest.api.prestador.Prestador;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
public class Especialidade {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false, unique = true)
    private long id;

    @Column(nullable = false)
    private String descricao;

    @ManyToMany(mappedBy = "especialidades")
    @JsonBackReference
    private List<Prestador> prestadores;
}
