package le.apps.gftest.api.especialidade.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EspecialidadeDTO {

    private long id;
    private String descricao;
}
