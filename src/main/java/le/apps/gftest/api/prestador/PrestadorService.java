package le.apps.gftest.api.prestador;

import com.fasterxml.jackson.core.JsonProcessingException;
import le.apps.gftest.api.especialidade.Especialidade;
import le.apps.gftest.api.prestador.dto.PrestadorDTO;
import le.apps.gftest.api.prestador.dto.PrestadorProximidadeDTO;
import le.apps.gftest.service.google.GoogleService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service
public class PrestadorService {

    @Autowired
    private PrestadorRepository prestadorRepository;

    @Autowired
    private GoogleService googleService;

    @Autowired
    private ModelMapper mapper;

    public List<Prestador> getPrestadoresPorEspecialidade(Especialidade especialidade) {

        List<Especialidade> especialidades = new ArrayList<>();
        especialidades.add(especialidade);

        return prestadorRepository.findByEspecialidadesIn(especialidades);
    }

    public List<PrestadorProximidadeDTO> getPrestadoresPorLatitudeLongitudeEspecialidade(double latitude, double longitude, Especialidade especialidade) throws JsonProcessingException {

        List<Especialidade> especialidades = new ArrayList<>();
        especialidades.add(especialidade);
        List<Prestador> prestadores = prestadorRepository.findByEspecialidadesIn(especialidades);

        List<PrestadorProximidadeDTO> prestadorProximidadeList = new ArrayList<>();

        for(Prestador p : prestadores) {

            long distance = googleService.getGoogleDirectionDistance(latitude, longitude, p.getLatitude(), p.getLongitude());

            PrestadorProximidadeDTO prestadorProximidade = mapper.map(p, PrestadorProximidadeDTO.class);
            prestadorProximidade.setDistanciaEmKm(distance);

            prestadorProximidadeList.add(prestadorProximidade);
        }

        // ordena por distancia
        prestadorProximidadeList.sort(Comparator.comparingLong(PrestadorProximidadeDTO::getDistanciaEmKm));

        return prestadorProximidadeList;
    }
}
