package le.apps.gftest.api.prestador;

import le.apps.gftest.api.especialidade.Especialidade;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
public class Prestador {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false, unique = true)
    private long id;

    @ManyToMany
    @JoinTable(name = "prestadores_especialidades", joinColumns = @JoinColumn(name = "id_prestador"), inverseJoinColumns = @JoinColumn(name = "id_especialidade"))
    private List<Especialidade> especialidades;

    @Column(nullable = false)
    private String nome;

    @Column(nullable = false)
    private String endereco;

    @Column(nullable = false)
    private String bairro;

    @Column(nullable = false)
    private String cidade;

    @Column(nullable = false)
    private Double latitude;

    @Column(nullable = false)
    private Double longitude;
}
