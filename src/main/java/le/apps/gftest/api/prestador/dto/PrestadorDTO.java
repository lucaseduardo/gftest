package le.apps.gftest.api.prestador.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PrestadorDTO {

    private long id;
    private String nome;
    private String endereco;
    private String bairro;
    private String cidade;
    private Double latitude;
    private Double longitude;
}
