package le.apps.gftest.api.prestador.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PrestadorProximidadeDTO {

    private String nome;
    private String endereco;
    private double latitude;
    private double longitude;
    private long distanciaEmKm;
}
