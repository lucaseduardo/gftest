package le.apps.gftest.api.prestador;

import com.fasterxml.jackson.core.JsonProcessingException;
import le.apps.gftest.api.especialidade.Especialidade;
import le.apps.gftest.api.especialidade.EspecialidadeService;
import le.apps.gftest.api.especialidade.dto.EspecialidadeDTO;
import le.apps.gftest.api.prestador.dto.PrestadorDTO;
import le.apps.gftest.api.prestador.dto.PrestadorProximidadeDTO;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/gftest/prestador")
public class PrestadorController {

    @Autowired
    private PrestadorService prestadorService;

    @Autowired
    private EspecialidadeService especialidadeService;

    @Autowired
    private ModelMapper mapper;

    @GetMapping(value = "/especialidade/{idEspecialidade}", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<PrestadorDTO>> getPrestadoresPorEspecialidades(@PathVariable("idEspecialidade") long idEspecialidade) {

        Especialidade especialidade = especialidadeService.getEspecialidade(idEspecialidade);
        List<Prestador> prestadores = prestadorService.getPrestadoresPorEspecialidade(especialidade);
        List<PrestadorDTO> prestadoresDTO = mapper.map(prestadores, new TypeToken<List<PrestadorDTO>>() {}.getType());

        return ResponseEntity.ok(prestadoresDTO);
    }

    @GetMapping(value = "/{latitude}/{longitude}/{idEspecialidade}", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<PrestadorProximidadeDTO>> getPrestadoresPorLatitudeLongitudeEspecialidade(@PathVariable("latitude") double latitude, @PathVariable("longitude") double longitude, @PathVariable("idEspecialidade") long idEspecialidade) throws JsonProcessingException {

        Especialidade especialidade = especialidadeService.getEspecialidade(idEspecialidade);
        List<PrestadorProximidadeDTO> prestadores = prestadorService.getPrestadoresPorLatitudeLongitudeEspecialidade(latitude, longitude, especialidade);

        return ResponseEntity.ok(prestadores);
    }
}
