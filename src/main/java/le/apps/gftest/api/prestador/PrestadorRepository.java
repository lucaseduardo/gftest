package le.apps.gftest.api.prestador;

import le.apps.gftest.api.especialidade.Especialidade;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PrestadorRepository extends CrudRepository<Prestador,Long> {

    List<Prestador> findByEspecialidadesIn(@Param("especialidade") List<Especialidade> especialidades);
}
