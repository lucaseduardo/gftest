package le.apps.gftest.service.google;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import le.apps.gftest.service.google.dto.DirectionsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class GoogleService {

    @Value("${google.key}")
    private String googleKey;

    @Value("${google.url.directions}")
    private String googleDirectionsUrl;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    public long getGoogleDirectionDistance(double originLat, double originLon, double destinationLat, double destinationLon) throws JsonProcessingException {

        StringBuilder sb = new StringBuilder();
        sb.append(googleDirectionsUrl).append("key=").append(googleKey);
        sb.append("&origin=").append(originLat).append(",").append(originLon);
        sb.append("&destination=").append(destinationLat).append(",").append(destinationLon);

        String response = restTemplate.getForObject(sb.toString(), String.class);
        DirectionsDTO directions = objectMapper.readValue(response, DirectionsDTO.class);

        if(directions.getRoutes() == null) return 0;
        if(directions.getRoutes().size() == 0) return 0;
        if(directions.getRoutes().get(0).getLegs() == null) return 0;
        if(directions.getRoutes().get(0).getLegs().size() == 0) return 0;
        if(directions.getRoutes().get(0).getLegs().get(0).getDistance() == null) return 0;

        return directions.getRoutes().get(0).getLegs().get(0).getDistance().getValue();
    }
}
