package le.apps.gftest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GftestApplication {

	public static void main(String[] args) {
		SpringApplication.run(GftestApplication.class, args);
	}

}
